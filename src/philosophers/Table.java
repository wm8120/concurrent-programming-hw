package philosophers;

import java.util.Random;

import static java.lang.System.out;

public class Table {
	int numSeats;
	Thread[] phils;
	final Random r = new Random();
	final int  timesToEat;
	static final int MAXMSECS = 1000;
	final Logger log;
	
	class Philosopher implements Runnable{
		final int seat;  //where this philosopher is seated, seats numbered from 0 to numSeats-1
	    int timesToEat;   
		
		Philosopher(int seat, int timesToEat){
			this.seat = seat;
			this.timesToEat = timesToEat;
		}
		
		void think(){
			log.thinks(seat);
			try{
				Thread.sleep(r.nextInt(MAXMSECS));
			}
			catch(InterruptedException e){
				/*ignore*/
			}
		}
		

		void eat(){
			log.eats(seat);
			try{
				Thread.sleep(r.nextInt(MAXMSECS));
			}
			catch(InterruptedException e){
				/*ignore*/
			}			
		}
			
		public void run(){
			for (;timesToEat > 0; --timesToEat){
				think();
				eat();
			}
		}
	}
	
	Table(int numSeats, int timesToEat, Logger log) throws InterruptedException{
		this.numSeats = numSeats;  //set the number of seats around the table.  Must be at least 2
		this.timesToEat = timesToEat;  //number of times each philosopher should eat
		this.log = log;
		phils = new Thread[numSeats];  //create a Thread for each philosopher
		for (int i = 0; i < numSeats; i++) phils[i] = new Thread(new Philosopher(i, timesToEat));	
	}
	
	void startDining(){
		for (int i = 0; i < numSeats; i++) phils[i].start();
	}
	
	void closeRestaurant() throws InterruptedException{
		for (int i = 0; i < numSeats; i++) phils[i].join();
	}
	
	
	
	public static void main(String[] args) throws InterruptedException{
		if (args.length < 2){
			out.println("usage:  java Table numSeats timesToEat");
			return;
		}
		int numPhils = Integer.parseInt(args[0]);
		int timesToEat = Integer.parseInt(args[1]);
		Logger log = new PrintLogger();
		Table table = new Table(numPhils, timesToEat, log);
		table.startDining();
		table.closeRestaurant();
	    System.out.println("restaurant closed.  Behavior was " + (log.isCorrect()?"correct":"incorrect"));
	}
}


