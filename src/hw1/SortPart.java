package hw1;

public class SortPart implements Runnable {
	long[] input_array;
	long[] scratch_array;
	int start_pos;
	int end_pos;
	int nprocs;

	public SortPart(long[] input, int start, int end, long scratch[], int arg){
		input_array = input;
		scratch_array = scratch;
		start_pos = start;
		end_pos = end;
		nprocs = arg;
	}

	@Override
	public void run(){
		MergeSortTest.parallelMergeSort(input_array, start_pos, end_pos, scratch_array, nprocs);
		System.arraycopy(input_array, start_pos, scratch_array, start_pos, end_pos-start_pos);
	}
}
