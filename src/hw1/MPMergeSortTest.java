package hw1;

import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;

import static java.lang.System.out;
import static java.lang.System.err;

import java.lang.Thread;
import java.util.Arrays;

public class MPMergeSortTest {

	private static int SAMPLES;
	private static int POINTS_PER_RUN;
	private static int INITIAL_SIZE;
	private static int NPROCS;

	private final static long SEED = 353L;

	public static long[] fillRandom(int size, long seed) {
		long[] data = new long[size];
		COP5618Random r = new COP5618Random(seed);
		for (int i = 0; i < data.length; ++i) {
			data[i] = r.nextLong();
		}
		return data;
	}

	public static boolean isSorted(long[] data, int start, int end) {
		for (int i = start + 1; i < end; i++) {
			if (data[i - 1] > data[i]) {
				System.out.println("Error:  i=" + i + " start= " + start
						+ " end=" + end + " data[i-1]=" + data[i - 1]
						+ " data[i]=" + data[i]);
				return false;
			}
		}
		return true;
	}

	public static void seqMergeSort(long[] input, int start, int end,
			long scratch[]) {
		int size = end - start;
		if (size < 2)
			return;
		int pivot = (end - start) / 2 + start;
		seqMergeSort(input, start, pivot, scratch);
		seqMergeSort(input, pivot, end, scratch);
		System.arraycopy(input, start, scratch, start, size);
		merge(scratch, input, start, pivot, end);
	}

	static void merge(long[] src, long[] dest, int start, int pivot, int end) {
		int l = start;
		int r = pivot;
		int i = start;
		while (l < pivot && r < end) {
			if (src[l] <= src[r])
				dest[i++] = src[l++];
			else
				dest[i++] = src[r++];
		}
		while (l < pivot) {
			dest[i++] = src[l++];
		}
		while (r < end) {
			dest[i++] = src[r++];
		}
	}

	public static void parallelMergeSort(final long[] input, final int start,
			int end, final long scratch[], final int arg) {
		if (arg <= 1){
			seqMergeSort(input, start, end, scratch);
		}
		else{
			int pivot = (end-start)/2+start;
			Thread left = new Thread(new SortPart(input, start, pivot, scratch, arg/2));
			Thread right = new Thread(new SortPart(input, pivot, end, scratch, arg/2));
			left.start();
			right.start();
			
			try {
				left.join();
				right.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			//System.arraycopy(input, start, scratch, start, end-start);
			merge(scratch, input, start, pivot, end);
		}
	}

	public static void main(String[] args) {
		long mainStartTime = System.nanoTime();
		if (args.length < 3){
			err.println("Not enougg arguments.  Need  INITIAL_SIZE POINTS_PER_RUN SAMPLES");
			return;
		}
		
		INITIAL_SIZE = Integer.parseInt(args[0]);
		POINTS_PER_RUN = Integer.parseInt(args[1]);
		SAMPLES = Integer.parseInt(args[2]); // number of times to run each test

		NPROCS = Runtime.getRuntime().availableProcessors();

		// initialize array containing sizes of arrays to sort. Double the size each time.
		int size[] = new int[POINTS_PER_RUN];
		for (int i = 0, s = INITIAL_SIZE; i < POINTS_PER_RUN; i++, s*=2) {
			size[i] = s;
		}

		// create arrays for holding measurements (nanosecs)
		long timedJavaSort[] = new long[POINTS_PER_RUN];
		long timedSeqSort[] = new long[POINTS_PER_RUN];
		long timedParallelSort[] = new long[POINTS_PER_RUN];

/* Repeat each test SAMPLES time and accumulate results. Divide by
 *    SAMPLES before printing to get average
 */
		
		// Use sorting routing from java.util.Arrays.sort
		SortMethod javaSortMethod = new JavaSortMethod();
		Thread javaSort = new Thread(new RunSort(size, SAMPLES, POINTS_PER_RUN, SEED, NPROCS, javaSortMethod, timedJavaSort));
		javaSort.start();

		// Sequential merge sort
		SortMethod seqMergeSortMethod = new SeqMergeSortMethod();
		Thread seqMergeSort = new Thread(new RunSort(size, SAMPLES, POINTS_PER_RUN, SEED, NPROCS, seqMergeSortMethod, timedSeqSort));
		seqMergeSort.start();
		
		// Parallel merge sort
		SortMethod paraMergeSortMethod = new ParaMergeSortMethod();
		Thread paraMergeSort = new Thread (new RunSort(size, SAMPLES, POINTS_PER_RUN, SEED, NPROCS, paraMergeSortMethod, timedParallelSort));
		paraMergeSort.start();

		try {
			javaSort.join();
			seqMergeSort.join();
			paraMergeSort.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// output formatted to be easily copied into a spreadsheet
		StringBuilder sb = new StringBuilder();
		OperatingSystemMXBean sysInfo = ManagementFactory
				.getOperatingSystemMXBean();
		sb.append("Architecture = ").append(sysInfo.getArch()).append('\n');
		sb.append("OS = ").append(sysInfo.getName()).append(" version ")
				.append(sysInfo.getVersion()).append('\n');
		sb.append("Number of available processors = ").append(NPROCS).append('\n');
		sb.append("size \t Java sort \t seq merge sort \t parallel merge sort \n");
		//divide by SAMPLES to get average.  
		for (int i = 0; i < POINTS_PER_RUN; ++i) {
			sb.append(size[i]).append('\t').
			   append(timedJavaSort[i] / SAMPLES ).append('\t'). 
			   append(timedSeqSort[i] / SAMPLES ).append('\t').
			   append(timedParallelSort[i] / SAMPLES ).append('\n');
		}
		System.out.println(sb);

		long mainEndTime = System.nanoTime();
		out.println("Total time:");
		out.println(mainEndTime-mainStartTime);
	}

}

class RunSort implements Runnable{
	//long[] data;
	int[] size;
	int samples;
	int points_per_run;
	final long seed;
	int nprocs;
	SortMethod sort_method;
	long[] timeRecord;
	
	public RunSort(int[] size, int samples, int points_per_run, final long seed, int nprocs, SortMethod sort_method, long[] timeRecord){
		//this.data = data;
		this.size = size;
		this.samples = samples;
		this.points_per_run = points_per_run;
		this.seed = seed;
		this.nprocs = nprocs;
		this.sort_method = sort_method;
		this.timeRecord = timeRecord;
	}
	
	@Override
	public void run(){
		out.print("parallel merge sort");
		for (int rep = 0; rep < samples; rep++) {
			for (int i = 0; i < points_per_run; i++) {
				long[] data = MergeSortTest.fillRandom(size[i], seed);
				long startTime = System.nanoTime();
				sort_method.sort(data, nprocs);
				//parallelMergeSort(data, 0, data.length, new long[data.length], NPROCS);
				timeRecord[i] += (System.nanoTime() - startTime);
				if (!MergeSortTest.isSorted(data, 0, size[i])) {
					err.println("Error: parallel merge sort");
				}
				out.print('.');
			}
		}
		out.println();

	}
}

interface SortMethod{
	public void sort(long[] data, int nprocs);
}

class JavaSortMethod implements SortMethod{
	@Override
	public void sort(long[] data, int nprocs){
		Arrays.sort(data);
	}
}

class SeqMergeSortMethod implements SortMethod{
	@Override
	public void sort(long[] data, int nprocs){
		MergeSortTest.seqMergeSort(data, 0, data.length, new long[data.length]);
	}
}

class ParaMergeSortMethod implements SortMethod {
	@Override
	public void sort(long[] data, int nprocs) {
		MergeSortTest.parallelMergeSort(data, 0, data.length, new long[data.length], nprocs);
	}
}